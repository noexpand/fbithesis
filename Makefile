# Set Variables
PACKAGE					:= fbithesis
EXAMPLE					:= example
GPG							:= gpg2
MV							:= mv
TAR							:= tar
GZIP						:= gzip
LPR							:= lpr
DISTKEY					:= 0xF4D24AC9
DISTKEYNAME			:= distribution.key
CVS     				:= svn
CVS_FLAGS       := -Q
CHECKOUT_CMD    := co
UPDATE_CMD      := up
UPDATE_FORCE    := -C
RELEASE_CMD     := release -d
PDFLATEX				:= pdflatex
BIBTEX					:= bibtex
MAKEINDEX				:= makeindex
DISTFILES = ${PACKAGE}.dtx ${PACKAGE}.pdf ${PACKAGE}.ins ${PACKAGE}.dtx.asc \
            ${DISTKEYNAME} README.txt example.tex example.pdf

# Remove standard suffix rules. They are useless with TeX.
.SUFFIXES:

.PHONY: test all
test : usage

all : usage

# Cleaning:
.PHONY: clean-rek clean checksum sign dist tar key gz
clean-rek : clean 

clean :
	-${RM} *.log *.aux *.dvi *.ps *~ *.bak *.bbl *.blg 
	-${RM} *.ilg *.ind *.idx *.toc *.glo *.gls *.flc
	-${RM} *.ins *.cls *.drv *.sum *.cfg *.eps
	-${RM} *.brf
	-${RM} lppl.txt README.txt example*

# Distribution section:
sign: ${PACKAGE}.dtx.asc

${PACKAGE}.dtx.asc: ${PACKAGE}.dtx
	${GPG} -ba --yes -u ${DISTKEY} ${PACKAGE}.dtx

key: ${DISTKEYNAME}

${DISTKEYNAME} :
	${GPG} --export -a ${DISTKEY} > $@

tar: ${PACKAGE}.tgz

${PACKAGE}.tgz: dist ${DISTKEYNAME}
	${TAR} czf $@ ${PACKAGE}.dtx ${PACKAGE}.dtx.asc ${DISTKEYNAME}

backup:
	${TAR} cf ${HOME}/stuff/tex/fbi.tar ${PACKAGE}.dtx ${PACKAGE}.bib\
                  ${PACKAGE}.gather.dtx ToDo.txt ${PACKAGE}.xml

dist: sign clean key

# Documentation section:
.PHONY: bib index glossar crossrefs doc

# BibTeX
bib : ${PACKAGE}.bbl

${PACKAGE}.bbl : ${PACKAGE}.aux
	${BIBTEX} ${PACKAGE}

# Makeindex
glossar : ${PACKAGE}.gls

${PACKAGE}.gls : ${PACKAGE}.glo
	${MAKEINDEX} $< -s docindex.ist -o $@

${PACKAGE}.glo : crossrefs

index : ${PACKAGE}.ind

${PACKAGE}.ind : ${PACKAGE}.idx
	${MAKEINDEX} $< -s docindex.ist -o $@

${PACKAGE}.idx : crossrefs

${PACKAGE}.drv : install

dtx : ${PACKAGE}.dtx
	-${PDFLATEX} $<

# Documentation
crossrefs : ${PACKAGE}.drv dtx
	-${PDFLATEX} $<
	-${PDFLATEX} $<
	-${PDFLATEX} $<

doc : ${PACKAGE}.pdf

${PACKAGE}.pdf : ${PACKAGE}.drv ${PACKAGE}.gls ${PACKAGE}.ind ${PACKAGE}.dtx
	${PDFLATEX} $<

${PACKAGE}.dvi : ${PACKAGE}.dtx ${PACKAGE}.gls ${PACKAGE}.ind
	${PDFLATEX} $<

# Example
.PHONY: example

example : ${EXAMPLE}.tex
	${PDFLATEX} $<

# Install section:
.PHONY: install

${PACKAGE}.ins : ${PACKAGE}.dtx
	-${RM} -f $@
	-${PDFLATEX} --interaction batchmode $<

install ${EXAMPLE}.tex : ${PACKAGE}.ins
	${PDFLATEX} $<

# Output section:
.PHONY : help version usage
help : version
	@echo 'Valid targets are:'
	@echo '         clean    : clean up dir'
	@echo '         clean-rek: clean up everything (rekursive)'
	@echo '         sign     : sign with software distribution key'
	@echo '         dist     : make new distribution'
	@echo '         tar      : make the tarball of the distribution'
	@echo '         backup   : write tarball to stuff/tex/'
	@echo '         index    : make the index of the documentation'
	@echo '         glossar  : make the glossar of the documatation'
	@echo '         docu     : make the documentation'
	@echo '         install  : install package'
	@echo '         example  : make example'
	@echo '         help     : show this help'
	@echo '         version  : show version info'

version : 
	@echo 'This is $$Id$ '

usage : version
	@echo 'Usage: make [targets] (make help for help)'

# DO NOT DELETE THIS LINE
